<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}
$boot = function () {
    try {
        $translator = \Hn\AutoTranslator\Utility\Helper::getTranslator();

        $translateToMessage = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('translateToMessage', 'auto_translator', ['%s', $translator->getName()]);

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(sprintf('TCEMAIN.translateToMessage = %s', $translateToMessage));
    } catch (\TYPO3\CMS\Extbase\Object\Container\Exception\UnknownObjectException $e) {
        // do nothing
    }
};

$boot();
unset($boot);

