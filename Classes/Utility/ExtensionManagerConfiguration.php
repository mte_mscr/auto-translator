<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 15.06.18
 * Time: 10:54
 */

namespace Hn\AutoTranslator\Utility;

use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;

class ExtensionManagerConfiguration
{
    /**
     * @param $propertyPath
     * @return string
     */
    public static function getProperty($propertyPath)
    {
        return ObjectAccess::getPropertyPath(self::getConfiguration(), $propertyPath);
    }

    /**
     * @return array
     */
    protected static function getConfiguration()
    {
        $typoscriptService = GeneralUtility::makeInstance(TypoScriptService::class);
        return $typoscriptService->convertTypoScriptArrayToPlainArray(
            unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['auto_translator'])
        );
    }
}