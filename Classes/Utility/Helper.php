<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 15.06.18
 * Time: 10:54
 */

namespace Hn\AutoTranslator\Utility;

use Hn\AutoTranslator\Service\TranslationService;
use Hn\AutoTranslator\Service\TranslatorInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class Helper
{
    /**
     * @return TranslatorInterface
     */
    public static function getTranslator()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);

        /** @var TranslationService $translationService */
        $translationService = $objectManager->get(TranslationService::class);

        return $translationService->getTranslator();
    }
}