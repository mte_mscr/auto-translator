<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 17.06.18
 * Time: 20:57
 */

namespace Hn\AutoTranslator\Service;

class TranslationService
{
    /**
     * @var \Hn\AutoTranslator\Service\TranslatorInterface
     * @inject
     */
    protected $translator;

    /**
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

}