<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 14.06.18
 * Time: 17:04
 */

namespace Hn\AutoTranslator\Service;

interface TranslatorInterface
{
    /**
     * @param string $content the content to be translated
     * @param string $targetLanguageIsoCode the target ISO 639-1 code
     * @param string|null $sourceLanguageIsoCode the source ISO 639-1 code
     * @return string
     */
    public function translate($text, $targetLanguageIsoCode, $sourceLanguageIsoCode = null);

    /**
     * @return string
     */
    public function getName();
}