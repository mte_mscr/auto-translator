<?php
/**
 * Created by PhpStorm.
 * User: kay
 * Date: 13.06.18
 * Time: 16:33
 */

namespace Hn\AutoTranslator\Hook;

use Hn\AutoTranslator\Utility\ExtensionManagerConfiguration;
use Hn\AutoTranslator\Utility\Helper;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Extbase\Object\Container\Exception\UnknownObjectException;
use TYPO3\CMS\Core\DataHandling\DataHandler as CoreDataHandler;

class DataHandler implements SingletonInterface
{
    protected $currentCommandInfo;

    /**
     * hook method
     *
     * store current command info
     *
     * @param $command
     * @param $table
     * @param $id
     * @param $value
     * @param CoreDataHandler $dataHandler
     * @param $pasteUpdate
     */
    public function processCmdmap_preProcess($command, $table, $id, $value, CoreDataHandler $dataHandler, $pasteUpdate)
    {
        $this->currentCommandInfo[spl_object_hash($dataHandler)] = [
            'command' => $command,
            'table' => $table,
            'id' => $id,
            'value' => $value,
            'pasteUpdate' => $pasteUpdate
        ];
    }

    /**
     * hook method
     *
     * @param $content
     * @param $targetLang
     * @param $dataHandler
     */
    public function processTranslateTo_copyAction(&$content, $targetLang, CoreDataHandler $dataHandler, $fieldName = null)
    {
        // refetch language record WITH language iscode
        $targetLang = BackendUtility::getRecord('sys_language', (int)$targetLang['uid'], 'uid, language_isocode, title');

        try {
            $content = Helper::getTranslator()->translate($content, $targetLang['language_isocode'], $this->getSourceLanguageIsocode($dataHandler));
        } catch (UnknownObjectException $e) {
            $this->logException($e);
        }
    }

    /**
     * @param \Exception $e
     */
    protected function logException(\Exception $e)
    {
        /** @var BackendUserAuthentication $backendUser */
        $backendUser = $GLOBALS['BE_USER'];
        $backendUser->writelog(4, 0, 1, 0, '[auto_translator] ' . $e->getMessage(), []);
    }

    /**
     * returns the source language ISO 639-1 code for the record in the current command info
     *
     * @param CoreDataHandler $dataHandler the data handler object
     * @return string|null the language ISO 639-1 code
     */
    protected function getSourceLanguageIsocode(CoreDataHandler $dataHandler)
    {
        // retrieve current command info
        if (!array_key_exists(spl_object_hash($dataHandler), $this->currentCommandInfo)) {
            return null;
        }

        $commandInfo = $this->currentCommandInfo[spl_object_hash($dataHandler)];

        $sourceLang = $this->getLanguageRecord($commandInfo['table'], $commandInfo['id']);
        if ($sourceLang !== null) {
            return $sourceLang['language_isocode'];
        }

        if ($defaultLanguageIsocode = ExtensionManagerConfiguration::getProperty('config.default_language_isocode')) {
            return $defaultLanguageIsocode;
        }

        return null;
    }

    /**
     * return the language record for the specified table and record uid
     *
     * @param string $table the table name
     * @param int $uid
     * @return array|null
     */
    protected function getLanguageRecord(string $table, int $uid)
    {
        // records from page table are always translated from the default language
        if ($table === 'pages') {
            return null;
        }

        $record = BackendUtility::getRecord($table, $uid);
        return BackendUtility::getRecord('sys_language', $record[$GLOBALS['TCA'][$table]['ctrl']['languageField']]);
    }
}