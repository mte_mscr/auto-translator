<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Auto translator',
    'description' => 'Base extension for auto translation',
    'category' => 'be',
    'author' => 'Kay Wienöbst',
    'author_email' => 'kay@hauptsache.net',
    'state' => 'stable',
    'uploadfolder' => 0,
    'clearCacheOnLoad' => 1,
    'version' => '1.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.5.1-8.7.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
